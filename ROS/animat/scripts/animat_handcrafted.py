#!/usr/bin/env python

# This nodes is an handcrafted surviving behavior.

import rospy

from std_msgs.msg      import String
from sensor_msgs.msg   import CompressedImage

from animat.msg import WheelCmd, ConsumeCmd, PhysiologyState

# If you need to protect a variable by a mutex...
from multiprocessing import Lock

# Then, as usual
import numpy as np
import scipy.signal
import cv2

rotation_speed    = .5
translation_speed = 2
near_duration     = 1.5  
level_bottom      = .1   # The bottom line at 10% of the height from the bottom.

class Behavior :

    def __init__(self) :

        self.detection_status    = None
        self.enter_near_time     = None
        
        self.search_msg = WheelCmd()
        self.search_msg.vel_left  = - rotation_speed 
        self.search_msg.vel_right =   rotation_speed

        self.near_msg = WheelCmd()
        self.near_msg.vel_left  = translation_speed
        self.near_msg.vel_right = translation_speed
        
        
        self.data_mutex = Lock()
        self.drive = 'thirsty' # This tells what to do.
        self.current_image = None

        self.sub_physio = rospy.Subscriber('/animat/physio', PhysiologyState,
                                           self.on_physio,     
                                           queue_size=1)
        
        self.sub_image = rospy.Subscriber('/animat_vision/compressed',  CompressedImage,
                                          self.on_image, queue_size = 1, buff_size=2**22) 
        self.pub_image = rospy.Publisher("/debug/compressed", CompressedImage, queue_size = 1)
        self.pub_wheel = rospy.Publisher("/animat/wheel_cmd", WheelCmd, queue_size = 1)
        self.pub_ingest = rospy.Publisher("/animat/consume_cmd", ConsumeCmd, queue_size = 1)
        self.pub_play = rospy.Publisher("/animat/server_cmd", String, queue_size = 1)

        self.state_machine = {'search': {'action' : self.action_search, 'transition' : self.from_search},
                              'reach':  {'action' : self.action_reach,  'transition' : self.from_reach},
                              'near':   {'action' : self.action_near,   'transition' : self.from_near},
                              'ingest': {'action' : self.action_ingest, 'transition' : self.from_ingest}}
                       
        
    def print_status(self):
        print(self.drive)

    def from_search(self):
        if self.detection_status is None:
            return 'search'
        if self.detection_status['w_pos'] is None:
            return 'search'
        if self.detection_status['bottom'] :
            return 'near'
        return 'reach'
        
    def from_reach(self):
        if self.detection_status is None:
            return 'search'
        if self.detection_status['w_pos'] is None:
            return 'search'
        if self.detection_status['bottom'] :
            return 'near'
        return 'reach'
        
    def from_near(self):
        if (self.enter_near_time is None) or ((rospy.Time.now() - self.enter_near_time).to_sec() < near_duration):
            return 'near'
        self.enter_near_time = None
        return 'ingest'

    def from_ingest(self):
        if self.detection_status is None:
            return 'search'
        if self.detection_status['bottom'] :
            return 'ingest'
        return 'search'

    def action_search(self): # Turns on the left
        self.pub_play.publish('play') # Really useful at start only.
        self.pub_wheel.publish(self.search_msg)

    def action_near(self):
        if self.enter_near_time is None:
            self.enter_near_time = rospy.Time.now()
        self.pub_wheel.publish(self.near_msg)
        
    def action_reach(self):
        if self.detection_status is None:
            return
        w = self.detection_status['w_pos']
        msg = WheelCmd()
        msg.vel_left  = (1 + w) * translation_speed
        msg.vel_right = (1 - w) * translation_speed
        self.pub_wheel.publish(msg)

    def action_ingest(self):
        with self.data_mutex :
            drive = self.drive
            
        self.pub_wheel.publish(WheelCmd()) # first, we stop.
        
        msg = ConsumeCmd()
        if drive == 'hungry' :
            msg.consume = 'eat'
        elif drive == 'thirsty' :
            msg.consume = 'drink'
        else:
            return
        self.pub_ingest.publish(msg)
    
    def compute_detection(self):
        image = None
        with self.data_mutex :
            drive = self.drive
            image = self.current_image
        if image is None:
            self.detection_status = None
            return

        r = image[..., 2].astype(float) # Red   channel in [0.0, 255.0]
        g = image[..., 1].astype(float) # Green channel in [0.0, 255.0]
        b = image[..., 0].astype(float) # Blue  channel in [0.0, 255.0]

        # g is very similar to the global luminosity of the image. So
        # if r > g+delta, the pixel is somehow red. The same stands
        # for b.
        delta = 20.0
        if drive == 'hungry' :
            detection = (r > (delta + g)).astype(float) # red detector
        else:
            detection = (b > (delta + g)).astype(float) # blue detector

        # We need to remove noisy detections from the image
        # (i.e. false colors due to jpg compression).
        detection = scipy.signal.medfilt2d(detection, 7)
        


        # If someone needs to see the detection... we publish it.
        if self.pub_image.get_num_connections() > 0:
            msg              = CompressedImage()
            msg.header.stamp = rospy.Time.now()
            msg.format       = "jpeg"
            msg.data         = np.array(cv2.imencode('.jpg', (detection * 255).astype(np.uint8))[1]).tobytes()
            self.pub_image.publish(msg)
        
        wsum = np.sum(detection, axis=0)
        detected = np.sum(detection)
        w, bottom = None, False
        if detected > 10:
            w = np.sum(wsum * np.arange(image.shape[1])) * (1/detected)
            w = w / (image.shape[1]) - .5    # w in [-.5, .5]
            bottom_line = int((1 - level_bottom) * image.shape[0])
            bsum = np.sum(detection[bottom_line,...]) # Sum of the bottom line.
            if bsum / image.shape[1] > .5:            # more than 50% of the bottomline is detected.
                bottom = True
        self.detection_status = {'w_pos': w, 'bottom': bottom}
            

    # This updates the most urgent need (drinking or eating)
    def on_physio(self, msg):
        with self.data_mutex :
            if msg.food > msg.water:
                self.drive = 'thirsty'
            else:
                self.drive = 'hungry'
                
    def on_image(self, msg):
        compressed_in = np.frombuffer(msg.data, np.uint8)
        with self.data_mutex :
            self.current_image = cv2.imdecode(compressed_in, cv2.IMREAD_COLOR)

            
    def loop(self):
        status = 'search'
        while not rospy.is_shutdown():
            self.compute_detection() # We update the image-based detections.
            status  = self.state_machine[status]['transition']() # We compute the new status...
            self.state_machine[status]['action']()               # ... and perform the corresponding action.
            
            rospy.sleep(0.1) # we sleep for 100ms

if __name__ == '__main__':     
    rospy.init_node('handcrafted')

    behavior_node = Behavior()
    behavior_node.loop()     

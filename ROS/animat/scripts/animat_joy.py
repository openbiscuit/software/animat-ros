#!/bin/sh
# -*- mode: python; -*-

# Shell commands follow to lauchn using the python with ROS_PYTHON_VERSION
# Next line is bilingual: it starts a comment in Python, and is a no-op in shell
""":"

# Find a suitable python interpreter (adapt for your specific needs)
cmd=/usr/bin/python${ROS_PYTHON_VERSION}
command -v > /dev/null $cmd && exec $cmd $0 "$@"

echo "OMG Python${ROS_PYTHON_VERSION} not found, exiting!!!!!11!!eleven" >2

exit 2

":"""
# Previous line is bilingual: it ends a comment in Python, and is a no-op in shell
# Shell commands end here
# -*- coding: utf-8 -*-

import rospy
from animat.msg import WheelCmd
from animat.msg import ConsumeCmd
from sensor_msgs.msg import Joy
from std_msgs.msg import String

## Joystick mapping for HAMA-X-style
# axes: [
#0   -1:droiteJL,1:gaucheJL,
#1   -1:arrièreJL,1:avantJL,
#2   -1:droiteJR,1:gaucheJR,
#3   -1:arrièreJR,1:avantJR,
#4   -1:RT,1LT
#5   -1:droitePL,1:gauchePL,
#6   -1:arrièrePL,1:avantPL]
#
# buttons: [1V,2R,3B,4J,LB,RB,LT,RT,9,10,JL,JR]

mapping = {
    'hama' : {
        'axis': { 'leftW':  1, 'rightW': 3, },
        'btn':  { 'deadman': 4, 'eat': 1, 'drink': 2, 'none': 0,
                  'reset': 8, 'play':9 }
    },
    'F710' : {
        'axis': { 'leftW':  1, 'rightW': 4, },
        'btn':  { 'deadman': 4, 'eat': 1, 'drink': 2, 'none': 0,
                  'reset': 8, 'play': 9}
    },
    'F310' : {
        'axis': { 'leftW':  1, 'rightW': 3, },
        'btn':  { 'deadman': 4, 'eat': 2, 'drink': 0, 'none': 1,
                  'reset': 8, 'play': 9}
    }
}

class AnimatJoyTeleop( object ):
    """Joystick to Animat ROS commands."""

    def __init__(self):
        """Initialize joystick."""
        # publishers
        self.wheel_cmd_pub = rospy.Publisher( '/animat/wheel_cmd', WheelCmd,
                                              queue_size=10 )
        self.cons_cmd_pub = rospy.Publisher( '/animat/consume_cmd', ConsumeCmd,
                                             queue_size=10 )
        self.server_cmd_pub = rospy.Publisher( '/animat/server_cmd', String, queue_size=10)
        
        # Mapping joystick and action
        m = rospy.get_param( "~joymap", "Hama" )
        rospy.loginfo( "AnimatJoy mappinf={}".format(m ))
        self.joy_map = mapping[m]     

        # internal publication flag to tell what we must send a last wheel_cmd
        self.last_pub = False
        # last time a ConsumeCmd was sent        
        self.last_time = rospy.get_time()
        # delta time, at least, between two consume cmd (in s)
        self.delta_time = 0.2

        self.running_status = 'paused'
        
    def joy_cbk(self, msg):
        """Use joy axis/button to publish to `/wheel_cmd`.
        Only if the `deadman` button is active.
        """
        wheel_cmd = WheelCmd()
        consume_cmd = ConsumeCmd()
        
        axis_map = self.joy_map['axis']
        btn_map = self.joy_map['btn']
        if msg.buttons[btn_map['deadman']]:
            wheel_cmd.header.stamp = rospy.Time.now()
            wheel_cmd.vel_left = msg.axes[axis_map['leftW']]
            wheel_cmd.vel_right = msg.axes[axis_map['rightW']]
            self.last_pub = True
            self.wheel_cmd_pub.publish( wheel_cmd )

            if rospy.get_time() - self.last_time > self.delta_time:
                if msg.buttons[btn_map['reset']]:
                    self.server_cmd_pub.publish( "reset" )
                    return
                elif msg.buttons[btn_map['play']]:
                    if self.running_status == 'paused':
                        self.running_status = 'running'
                        self.server_cmd_pub.publish( "play" )
                    elif self.running_status == 'running':
                        self.running_status = 'paused'
                        self.server_cmd_pub.publish( "pause" )
                    return
                elif msg.buttons[btn_map['eat']]:
                    consume_cmd.header.stamp = rospy.Time.now()
                    consume_cmd.consume = 'eat'
                    self.last_time = rospy.get_time()
                    self.cons_cmd_pub.publish( consume_cmd )
                elif msg.buttons[btn_map['drink']]:
                    consume_cmd.header.stamp = rospy.Time.now()
                    consume_cmd.consume = 'drink'
                    self.last_time = rospy.get_time()
                    self.cons_cmd_pub.publish( consume_cmd )
                elif msg.buttons[btn_map['none']]:
                    consume_cmd.header.stamp = rospy.Time.now()
                    consume_cmd.consume = 'none'
                    self.last_time = rospy.get_time()
                    self.cons_cmd_pub.publish( consume_cmd )

        elif self.last_pub:
            # by default, cmd is 0,0 so publish
            wheel_cmd.header.stamp = rospy.Time.now()
            self.wheel_cmd_pub.publish( wheel_cmd )
            self.last_pub = False

    def run(self):
        """Initialize subscriber and start node."""
        self.joy_sub = rospy.Subscriber('/joy', Joy, self.joy_cbk)
        rospy.spin()
        
def main():
    """Start node."""
    # initialize ROS node
    rospy.init_node('animat_joy')
    # create instance and run node
    ajt = AnimatJoyTeleop()
    ajt.run()


if __name__ == '__main__':
    main()

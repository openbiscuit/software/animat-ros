# Animat-ROS avec un docker

Cette aide devrait vous permettre d'utiliser Animat-ROS sans avoir besoin d'installer ROS car ce dernier tournera dans un `docker` (voir [site web docker](https://docs.docker.com/)). 

Le principe général est donc le suivant: ROS tournera dans un `docker` en utilisant le module Animat-ROS qui sera situé dans le système de fichier de votre ordinateur.

![animat-ROS-docker](animat_ros_docker_smaller.png)

## Quelques mots rapides sur docker
`docker` permet de faire tourner des processus dans un environnement isolé au sein de votre ordinateur. Ces processus peuvent s'appuyer sur une image avec un OS différent de celui de votre machine, comme une sorte de *machine virtuelle légère*. Ici, nous utiliserons une image s'appuyant sur ubuntu20.04 avec ROS installé.

Un docker contient son propre système de fichier, et à chaque fois que vous relancez le docker, son système de fichier est dans l'état de départ de l'image. Il n'y a **pas de persistence**: si vous modifier des fichier dans le docker en train de s'exécuter, la prochaine fois que vous démarrez votre docker, vos modifications seront perdues !!

La **solution** proposée est de lancer un docker en lui disant qu'il utilisera une partie du système de fichier de votre ordinateur (la machine hôte du docker). Ainsi les modificiations faites dans cette **sous-partie commune** des deus systèmes de fichier (celui du docker et celui de votre PC) seront persistentes. Même quand le docker sera arrêté.

Les explictions sont données pour `ROS2`, mais la marche à suivre est presque la même pour `ROS`. Il suffira souvent (sauf mentionné explicitement), de remplacer `ros2` par `ros` un peu partout :o)

## Marche à suivre
### Préparer la machine hôte
- installer `docker` et récupérer (ou construire) l'image contenant Ubuntu_20.04+ROS2.
- récupérer le simulateur de l'environnement (Animat-Unity)
- préparer un workspace ROS2 avec le package Animat-ROS. Ce workspace ROS2 sera partagé entre votre PC et le docker en train de s'exécuter.

#### Installer docker
Voir la [doc en ligne](https://docs.docker.com/get-docker/)

Si vous avez installé correctement `docker`, vous devriez maintenant pouvoir le tester, en faisant par exemple :

``` bash
$> docker --version
Docker version 24.0.5, build ced0996

$> docker run hello-world

Hello from Docker!
This message shows that your installation appears to be working correctly.
[...]
```

#### Récupérer OU Construire l'image docker contenant Ubuntu+ROS

Pour récupérer l'image : TODO (sur un `registry`, ou un tar de 1.3 Go ??)

Pour construire l'image. Il faut utiliser le fichier `Dockerfile` du réperoire `ros2_docker` du git.

``` bash
$> cd $ANIMAT_ROS/ros2_docker

$> make build
```

L'image docker (`ros2-foxy:v1` ou `ros-base-noetic:v1` est construite et disponible dans le docker.

#### Récupérer ET installer le simulateur Animat+Unity

Vous devez aussi disposer du simulateur, voir [la page dédiée](https://biscuit.gitlabpages.inria.fr/software/animat-unity/). En suivant ces instructions, vous allez récupérer une archive à extraire. Pour l'exemple, on va dire que l'extraction crée le répertoire `$ANIM_UNITY` quelque part **sur la machine hôte (le système de fichier de votre PC)**. Dans ce répertoire, vous y trouverez l'exécutable de la simulation de l'environnement.

#### Préparer un worskpace ROS2 qui sera partagé avec l'image docker

Vous devez créer/avoir un répertoire qui sera le `worskpace` de ROS2. Désignons ce répertoire par la variable `$ROS2_WS`. Ce répertoire aura la structuration suivante.

```
$ROS2_WS
 |
 + modules
 | |
 | + animat-ros
 |
 + src
   |
   + lien vers ../modules/animat-ros/ROS2/animat
```


Dans ce répertoire, il faut installer le package `animat-ros` pour que le docker puisse l'utiliser. Voici un exemple de commandes pour cela

``` bash
$> cd $ROS2_WS
$> mkdir modules src
$> cd modules

$>  git clone git@gitlab.inria.fr:biscuit/software/animat-ros.git
OU
$>  git clone https://gitlab.inria.fr/openbiscuit/software/animat-ros.git

$> cd ../src
$> ln -s ../modules/animat-ros/ROS2/animat
```

### Lancer le docker et la simulation.

On exécute l'image docker, en partageant le workspace (techniquement, c'est un montage du système de fichier en `bind`) et en mappant le port 10000 qui est le port utilisé pour faire le lien entre `animat-unity` et `animat-ros` par le biais d'un noeud `ros-tcp` de `UnityRobotics` (avec `ROS`, c'est le port 9090 qui est partagé et on utilise un noeud `rosbridge`).

**Exécuter l'image docker (Machine hôte)**

Le plus simple est d'utiliser le `makefile` qui se trouve danas `$ANIMAT_ROS/ros2_docker`. En s'assurant que la variable `DOCKER_IMAG_NAME` du makefile correspond bien à votre configuration.

``` bash
make run
```

Ensuite, vous n'avez plus qu'à utiliser `ROS2` comme d'habiture.

**IMPORTANT** **Dans le docker**, lors de votre première utilisation, il ne faut pas oublier de faire

``` bash
bash init_ROS-TCP-Endpoint.sh
```

pour installer le noeud `ROS-TCP-Endpoint` dans votre workspace. Ensuite, comme souvent, on passe par un

``` bash
source $ROS2_WS/install/setup.bash
```

et vous pouvez ensuite vous lancer sous ros (voir le README.md principal de `$ANIMAT_ROS`), par exemple:
``` bash
# compiler le package animat-ros
root@dockerID:/ros2-ws# colcon XXXXXXX

# lancer animat-ros, option AVEC ou SANS clavier, joystick, etc
root@dockerID:/ros2-ws# roslaunch animat animat_server.launch use_keyboard:=true
# OU
root@dockerID:/ros2-ws# roslaunch animat animat_server.launch use_joystick:=true joystick_map:=F710 joystick_dev:=/dev/input/js0
```

**Lancer la simulation, dans un autre terminal (machine hôte)**

``` bash
$> cd $ANIM_ROS
$> ./animat_0.4.x86_64  ## version actuelle du simulateur
```

Si vous en avez fini avec votre docker, `Ctrl-D` vous fera quitter votre container. Le docker tourne toujours, vous pouvez d'ailleurs le rejoindre avec `make join`. Si vous voulez arrêter le docker, c'est `make kill`. Et pour enlever l'image docker (si par exemple vous voulez la reconstruire) c'est `make remove`.


**NOTE** cette partie sont des rappels pour ROS (**pas ROS2**)

``` bash
source /opt/ros/noetic/setup.bash
```

par la suite, quand vos modules sont compilés avec `catkin`, il faudra sans doute faire

``` bash
source ~/ros-ws/devel/setup.bash
```

et vous pouvez ensuite vous lancer sous ros (voir le README.md principal de `$ANIMAT_ROS`), par exemple:
``` bash
# compiler le package animat-ros
root@dockerID:/ros-ws# catkin build

# lancer animat-ros, option AVEC ou SANS clavier, joystick, etc
root@dockerID:/ros-ws# roslaunch animat animat_server.launch use_keyboard:=true
# OU
root@dockerID:/ros-ws# roslaunch animat animat_server.launch use_joystick:=true joystick_map:=F710 joystick_dev:=/dev/input/js0
```
### explication du makefile pour les plus curieux
En particulier pour `make run`

Le problème principal est de vous créer un utilisateur dans le docker qui puisse créer et manipuler les fichier de `ros_ws` comme si c'était vous. Il faut donc créer cet utilisateur dans l'image docker à la volée.

On procéde en trois étapes:

``` bash
	docker run \
	--name ${DOCKER_NAME} \
	--detach --tty --interactive \
	--env DISPLAY=$DISPLAY \
	--volume /tmp/.X11-unix/:/tmp/.X11-unix \
    --publish 10000:10000 \
	--mount type=bind,src=${HOST_ROS_WS},target="/home/dev_ws" \
	${DOCKER_IMG} bash; \
```
Cela crée un container docker qui utilise notre image `${DOCKER_IMG}`. Ce docker tourne en mode **détaché** (`option --detach`) et a pour petit nom `${DOCKER_NAME}` (`ros2dock` pour ROS2). Ce container tourne avec les options suivantes:

- --env DISPLAY=$DISPLAY et ---volume /tmp/.X11-unix/:/tmp/.X11-unix : permet d'autoriser des application avec GUI (si `xhost+` sur la machine hôte).
- --tty --interactive : mode interactif avec un terminal (mas détaché, ce qui permet de faire que le docker contine de tourner)
- --publish 10000:1000 : mapping du port 10000 pour le `ROS-TCP-Endpoint`. (en `ROS1` mapping du port 9090 du container du docker avec le port 9090 de `localhost` (127.0.0.1) de la machine hôte).
- --mount type=bind,src=${HOST_ROS_WS},target="/home/dev_ws" : montage de la portion du système de fichier avec le workspace ROS de manière partagée, `/home/dev_ws` sera ensuite partagé avec le `user` final en tant que `ros2-ws`.

Ensuite, il faut créer l'utilisateur adéquat dans ce docker. Cela se fait en executant le script `set_user_env.sh` avec les paramètres `UID user_name GID group_name`. Ce script crée aussi un `/home/user_name` avec un lien vers `/ros-ws`.
Cet utiliateur n'a pas de mot de passe, et peut utiliser `sudo` dans le docker.

``` bash
docker exec \
    --detach rosdock \
    /root/set_user_env.sh ${USER_ID} ${USER_NAME} ${GROUP_ID} ${GROUP_NAME};
```

Enfin, on crée une session interative avec le docker en prenant le rôle de cet utilisateur qu'on vient de créer.

``` bash
sleep 1; docker exec \
	--user ${USER_NAME} \
    --workdir /home/${USER_NAME} \
    --tty --interactive \
    rosdock bash
```

La commande `sleep` est là pour s'assurer que docker a bien eu le temps d'exécuter la commande précédente.


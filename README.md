# Animat-ROS

ROS packages for the Animat Unity environment.

## Dépendances

Package testé avec `ros-melodic` (ubuntu 18.04) et `ros-noetic` (ubuntu 20.4). La différence ESSENTIELLE est que `melodic` s'appuie sur `python2` alors que `noetic` sur `python3`.

Il faut installer quelques dépendances ROS :

```
sudo apt install ros-xxx-rosbridge-server ros-xxx-joy
```

Vous devez aussi disposer du simulateur, voir [la page dédiée](https://openbiscuit.gitlabpages.inria.fr/software/animat-unity/). En suivant ces instructions, vous allez récupérer une archive à extraire. Pour l'exemple, on va dire que l'extraction crée le répertoire `$ANIM_UNITY` quelque part. Dans ce répertoire, vous y trouverez l'exécutable de la simulation de l'environnement. 

## Utilisation 

Il faut cloner ce git, disons dans `$ANIM_ROS` et faire un lien symbolique dans votre workspace ROS :

```
git clone git@gitlab.inria.fr:openbiscuit/software/animat-ros.git
cd $ROS_WS/src
ln -s $ANIM_ROS/ROS/animat
cd ..
catkin build
```

Le fichier `ROS/animat/launch/animat_server.launch` permet de lancer Rosbridge et un node simple pour interagir avec le simulateur, soit avec un joystick, soit avec un clavier. Par exemple, si tout est bien installé : 

```
roslaunch animat animat_server.launch use_keyboard:=true
roslaunch animat animat_server.launch use_joystick:=true joystick_map:=F710 joystick_dev:=/dev/input/js0

```

Si vous souhaitez lancer le serveur rosbridge à la main, vous pouvez simplement lancer `roslaunch rosbridge_server rosbridge_websocket.launch port:=9090`

Il faut ensuite lancer le simulateur, dans une autre fenêtre :

```
cd $ANIM_UNITY/
./animat_x.x.x86_64
```

## Démo avec contrôleur fait-main

Il y a un controlleur fait main (handcrafted) fourni dans ce package. Il est surtout un exemple d'interaction avec les capteurs et effecteurs du robot.

Pour le lancer :
```
roslaunch animat animat_handcrafted.launch
```

Il faut ensuite lancer le simulateur, dans une autre fenêtre :

```
cd $ANIM_UNITY/
./animat_x.x.x86_64
```

Enfin, via des clients comme `rqt_image_view`, on peut visualiser les topics `/animat_vision/compressed` pour voir ce que voit le robot, et surtout `/debug/compressed` pour voir la détection de couleur réalsée au sein du contrôleur.



## Ressources

Le dépot git de Animat-Unity est [ici](https://gitlab.inria.fr/biscuit/software/animat-unity) mais n'est pas nécessaire pour lancer la simulation.

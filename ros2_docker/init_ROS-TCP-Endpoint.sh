#!/usr/bin/env bash

if [ -d ${HOME}/ros2-ws/src/ros_tcp_endpoint ]; then
    echo "**WARNING**: ${HOME}/ros2-ws/src/ros_tcp_endpoint already exists!"
    echo "  +-> remove it if you REALLY want to reinstall"
    exit
fi

#Check out ROS-TCP-Endpoint, ROS2 version
git clone https://github.com/Unity-Technologies/ROS-TCP-Endpoint ${HOME}/ros2-ws/src/ros_tcp_endpoint -b ROS2v0.7.0

# Build the workspace
cd ${HOME}/ros2-ws && . /opt/ros/${ROS_DISTRO}/setup.sh && colcon build

# Reference script with commands to source workspace
# COPY ./source_ros.sh /home/dev_ws/source_ros.sh

# # Change to workspace on sign in
# RUN echo "cd home/dev_ws" >> ~/.bashrc

# This is Dockerfile for ros2:ros-base for Foxy
# taken from https://github.com/Unity-Technologies/Unity-Robotics-Hub.git

FROM ros:foxy-ros-base

# install some additional tools
RUN apt-get update && apt-get install -q -y --no-install-recommends \
    aptitude \
    byobu \
    less \
    tree \
    vim \
    && rm -rf /var/lib/apt/lists/*

# Make ROS2 Workspace Dirs
RUN mkdir -p /home/dev_ws

# Right now, do not add ROS-TCP-Endpoint or Animat packages... To test!
# #Check out ROS-TCP-Endpoint, ROS2 version
# RUN git clone https://github.com/Unity-Technologies/ROS-TCP-Endpoint /home/dev_ws/src/ros_tcp_endpoint -b ROS2v0.7.0

# # Reference script with commands to source workspace
# COPY ./source_ros.sh /home/dev_ws/source_ros.sh

# # Change to workspace on sign in
# RUN echo "cd home/dev_ws" >> ~/.bashrc

# # Build the workspace
# RUN cd home/dev_ws && . /opt/ros/foxy/setup.sh && colcon build

# # Source the workspace on sign in
# RUN echo ". install/local_setup.bash" >> ~/.bashrc

# in order to create a suitable environment for a 'dynamical' user
# set at run time, need to install a special shell
COPY --chmod=744 set_user_env.sh /root/set_user_env.sh
# and a shell to install and build ROS-TCP-Endpoint
COPY --chmod=744 init_ROS-TCP-Endpoint.sh /usr/local/bin/init_ROS-TCP-Endpoint.sh

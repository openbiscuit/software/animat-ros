# This is an auto generated Dockerfile for ros:ros-base
# generated from docker_images/create_ros_image.Dockerfile.em
FROM ros:noetic-ros-core-focal

# install bootstrap tools
RUN apt-get update && apt-get install --no-install-recommends -y \
    build-essential \
    python3-rosdep \
    python3-rosinstall \
    python3-vcstools \
    && rm -rf /var/lib/apt/lists/*

# bootstrap rosdep
RUN rosdep init && \
  rosdep update --rosdistro $ROS_DISTRO

# install ros packages
RUN apt-get update && apt-get install -y --no-install-recommends \
    ros-noetic-ros-base=1.5.0-1* \
    && rm -rf /var/lib/apt/lists/*

# install additional ros packages
RUN apt-get update && apt-get install -y --no-install-recommends \
    ros-noetic-rosbridge-suite \
    python3-catkin-tools \
    && rm -rf /var/lib/apt/lists/*

# and some tools
RUN apt-get update && apt-get install -q -y --no-install-recommends \
    aptitude \
    byobu \
    vim \
    && rm -rf /var/lib/apt/lists/*
# make sur byobu doc is installed, despite this "minimal" environment
COPY help* /usr/share/doc/byobu

# in order to create a suitable environment for a 'dynamical' user
# set at run time, need to install a special shell
COPY --chmod=744 set_user_env.sh /root/set_user_env.sh
RUN mkdir -p /ros-ws
WORKDIR /root

#!/usr/bin/env bash

# set up a "proper" working environment for a new users
# - uid, gid and /home/$user
# - sudo capability
# - link from /home/$user to /ros-ws

# need 4 arguments
if [ "$#" -ne 4 ]; then
    echo "$0 : not enough argument <name> <ip>"
fi

user_id=$1
user_name=$2
group_id=$3
group_name=$4

# create user
echo "will create user ${user_name}:${user_id} in group ${group_name}:${group_id}"
groupadd --gid $group_id $group_name
useradd --create-home --shell /usr/bin/bash --gid $group_id --uid $user_id $user_name
passwd -d $user_name
addgroup $user_name sudo

# make link to /ros-ws
ln -s /ros-ws /home/$user_name/ros-ws
chown ${user_name}:${group_name} /home/$user_name/ros_ws

## ./set_user_env.sh 32925 dutech 200208 biscuit
